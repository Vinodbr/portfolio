const path = require('path');

module.exports = function override(config) {
  config.resolve = {
    ...config.resolve,
    alias: {
      ...config.alias,
      '@components': path.resolve(__dirname, 'src/components'),
      '@Header': path.resolve(__dirname, 'src/components/header'),
      '@Images': path.resolve(__dirname, 'public/images'),
      '@Banner': path.resolve(__dirname, 'src/components/banner'),
      '@Home': path.resolve(__dirname, 'src/components/home'),
      '@Connect': path.resolve(__dirname, 'src/components/connect'),
      '@Testimonials': path.resolve(__dirname, 'src/components/testimonials'),
      '@Fun': path.resolve(__dirname, 'src/components/fun')
    },
  };

  return config;
};