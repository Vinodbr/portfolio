import React from "react";
import "./styles.css";
import Home from "@Home/Home";
import Header from "@Header/Header";
import Connect from "@Connect/connect";
import Testimonials from "@Testimonials/testimonials";
import Fun from "@Fun/fun";
import { makeStyles } from "@material-ui/core/styles";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { createTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import '@fontsource/roboto';

const theme = createTheme({
  palette: {
    type: 'dark',
    primary: {      
      main: '#fbce07',
    },
    secondary: {
      main: '#333',
    },

    /*background:{
      paper: "#fbce07", default: "#fbce07"
    }*/
  }
});

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
    "& > *": {
      margin: 0,
    }
  },
});

export default function App() {
  const classes = useStyles();
  return (
    <ThemeProvider theme={theme}>
    <Router>
      <div className={classes.root}>
        <Header />
        <Switch>
          <Route exact from="/" render={(props) => <Home {...props} />} />
          <Route exact path="/connect" render={(props) => <Connect {...props} />}/>
          <Route
            exact
            path="/testimonials"
            render={(props) => <Testimonials {...props} />}
          />
          <Route exact path="/fun" render={(props) => <Fun {...props} />} />
        </Switch>
      </div>
    </Router>
    </ThemeProvider>
  );
}
