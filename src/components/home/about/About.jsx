import React from "react";
import { Typography } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  skill:{
    width:"30px",
    height:"30px",
    marginRight:theme.spacing(3)
  },
  reactjsSkill:{
    width:"45px",
    height:"35px",
    marginRight:theme.spacing(3)
  }

}));
const About = (props) => {
  const classes=useStyles();
  const parentClasses=props.classes;
  return (
    <Paper elevation={5} className={parentClasses.withBg} >
                <Typography color="primary" variant="subtitle2" >
                  About
                </Typography>
                <Typography variant="body2" paragraph >
                  I am a pragmatic programmer with 15 years of experience with
                  Java, Python, Reactjs, Nodejs stacks. Led various teams sizing
                  upto 20+ members and delivered mission-critical projects.
                  Highly skilled at Peopleware, Software Development Lifecycle,
                  Agile Development, Cloud computing on AWS cloud with
                  serverless technologies.
                </Typography>
                <Typography variant="body2" paragraph gutterBottom>
                  I split my time between managing people and collaborating with
                  the Director or VP of Engineering on behind-the-scenes
                  operational fun. When I am not an Individual Contributor, or
                  doing operational work, I dedicate my time to my direct
                  reports, making sure they are happy, engaged, learning and
                  have what they need to be highly functional Contributors.
                </Typography>
                <img src="icons/java.png" alt="java" className={classes.skill}/>
                <img src="icons/python.png" alt="python" className={classes.skill}/>
                <img src="icons/reactjs.png" alt="reactjs" className={classes.reactjsSkill}/>
                <img src="icons/nodejs.jpg" alt="nodejs" className={classes.skill}/>
                <img src="icons/aws.png" alt="aws" className={classes.reactjsSkill}/>
              </Paper>
  );
};
export default About;