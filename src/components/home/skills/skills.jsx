import React from "react";
import { Typography } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import { useTheme } from "@material-ui/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },

  skill:{
      marginTop: theme.spacing(2)
  },

  paper:{
      backgroundColor:'black',
  },
  
  

  [theme.breakpoints.up("sm")]: {
      divider: {
      backgroundColor:theme.palette.primary.main,
      height:theme.spacing(45),
      marginTop:theme.spacing(8),
    }
  },

  [theme.breakpoints.only("sm")]: {
    divider: {
        backgroundColor:theme.palette.primary.main,
        height:"1px",
        marginTop:"1px",
        width:theme.spacing(40),
        marginLeft:theme.spacing(1),
  }
},

  [theme.breakpoints.down("xs")]: {
    divider: {
    backgroundColor:theme.palette.primary.main,
    width:theme.spacing(40),
    marginLeft:theme.spacing(1),
  }
}

}));


const itemdata= [
    {
        title:"AWS Cloud",
        subtitles: 
        [
            {
            title:"Serverless:",
            text:"API Gateway, Lambda, Beanstalk, ECS-with spot instances",
            },
            {
            title:"Network & Compute:",
            text:"VPC, Subnets, Load balancer, Security groups, EC2",
            },
            {
                title:"Security:",
                text:"IAM’s, Cognito, Amplify, Store: EBS, EFS, S3, ECR",
            },
            {
                title:"Monitoring:",
                text:"Cloudwatch, CloudTrail",
            },
            {
                title:"Self hosted environment:",
                text:"AWS codepipeline, AWS cloud9, AWS codestar Others: Amazon MQ, Hybrid Cloud with knowledge on AWS transit gateway and AWS direct connect ",
            }
            
        ],
        divider:true
    },

    {
        title:"",
        subtitles: 
        [
            {
            title:"Languages:",
            text:"Java, Python, Javascript, HTML CSS",
            },
            {
            title:"Frameworks:",
            text:"Spring boot, Hibernate, ReactJS with Material UI, ExpressJS/NodeJS,Fast API, Powermock, Coverity, Cobertura",
            },
            {
                title:"API & Methodologies:",
                text:"Restful Web services(REST API), Swagger with Open API specification.Websockets, Caching techniques.Monitoring, Loggers and Contextual troubleshooting",
            },
            {
                title:"Database:",
                text:"PostgreSQL with postgis geometry plugin and Full text search, Partitioning and Sharding techniques",
            },
            {
                title:"Event bus:",
                text:"RabbitMQ, Python Celery consumer",
            },
            {
                title:"CI/CD:",
                text:"Bitbucket, Jenkins",
            }            
        ],
        divider:true
    },
    {
        title:"Others",
        subtitles: 
        [
            {
            title:"Security:",
            text:"Cookie, Token Based- JWT, OAuth, SSO",
            },
            {
            title:"Load Balancer:",
            text:"nginx, apache2 proxy, HA, fault tolerant",
            },
            {
                title:"Dev  & Build Tools:",
                text:"Git, Visual Studio, STS, Gradle, Maven, NPM, Python virtual environment, Shell scripts, Jmeter",
            },
            {
                title:"Management Tools:",
                text:"Jira, Confluence, Slack",
            },
            {
                title:"Operating System:",
                text:"Linux, Windows",
            },
            {
                title:"To Upskill:",
                text:"AWS EKS, NoSQL, Apache Spark, Event Streaming with Kafka and Microservices. Data orchestrator like Airflow, Dagster etc"
            }
            
        ],
        divider:false
    }
]

const Skills = (props) => {
  const classes = useStyles();
  const theme = useTheme();  
  const isMobile = useMediaQuery(theme.breakpoints.between("xs","sm"));
  return ( 
      <>
      <Typography  color="primary" variant="subtitle2" gutterBottom className={classes.skill}>Skills:</Typography>
    <Grid container spacing={1}>
   
       
          
          {itemdata.map((value,index) => {
                const title = value.title;
                const subtitles = value.subtitles;
                const isDivider = value.divider;
                return (
                    <>
                     <Grid item xs={12} sm={12} md={3} key={index}>        
                    <Paper elevation={5} className={classes.paper}>
                    <Typography key={index} variant="subtitle2" paragraph >
                    {title}
                  </Typography>
                  {subtitles.map((subtitle,subindex) => {

                    return (<span key={subindex}>
                        <Typography variant="subtitle2" color="primary">
                        {subtitle.title}
                      </Typography>   
                    <Typography  variant="body2" paragraph >{subtitle.text}</Typography>
                    </span>
                    );
                  })}
                </Paper>      
                </Grid>
                {isDivider?(<>
                    <Grid item xs={12} sm={12} md={1}>
                    <Paper elevation={5} className={classes.paper}>
                    <Divider  orientation={isMobile?"horizontal":"vertical"} className={classes.divider} variant="middle"/>
                    </Paper>
                    </Grid>                
                </>):(<></>)}
                    
                  </>
                );
              })}
    </Grid>
    </>
  );
};
export default Skills;
