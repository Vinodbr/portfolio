import React from "react";
import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";

const itemsdata = [
    {
        text:"Project Estimates, LLD, Sprint Planning ,Work Breakdown and Execution."
    },
    {
        text:"System Design - Event driven Architecture with high Scalability and Resiliency."
    },
    {
        text:"Communication and alignment with cross teams and visibility to higher management."
    },
    {
        text:"CI/CD pipelines - Develop, Build , deploy with docker container tech stack."
    },
    {
        text:"Enabling faster release processes driven by the development team with less deployment failures, No DevOps."
    },
    {
        text:"Review pull requests and work on features whenever I have bandwidth."
    },
    {
        text:"Highly Experienced in AWS cloud serverless technologies."
    },
    {
        text:"Experienced in handling multiple projects."
    },
    {
        text:"Tech / Solution deck presentation to project stakeholders."
    },
    {
        text:"Recently started taking a self hosted services route to enable faster development cycles."
    },

]

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        marginLeft:theme.spacing(0)       
      },
      div:{
        marginLeft:theme.spacing(1)
      },    
      list:{
          marginTop :"-20px",
          marginLeft: "-5px"
      },
      listItem:{
        display : "list-item",
        marginBottom: "-35px",
        listStyleType: "square",
        marginLeft: "25px"
      },

      [theme.breakpoints.down("sm")]: {
        list:{
            marginTop :"-15px",
            marginLeft: "-3px"
        },
        listItem:{
          display : "list-item",
          marginBottom: "-30px",
          listStyleType: "square",
          marginLeft: "20px"
        },
      },
      
     
  }));
const Whatido = (props) => {
  
  const classes = useStyles();
  
  return (
    <div className={classes.root} >
    <Paper  elevation={0}>
    <Typography component="div" className={classes.div} color="primary" variant="subtitle2" gutterBottom>
      What I do:
    </Typography>
    <List component="ul" className={classes.list} aria-label="what i do">      
      {itemsdata.map((value,index) => {
            const text= value.text;
           return(<ListItem key={index} className={classes.listItem}>
            <ListItemText>         
            <Typography variant="body2" paragraph gutterBottom >
                {text}
            </Typography>
            </ListItemText>
        </ListItem>)
        })}
    </List>
  </Paper>
  </div>
  )
};

export default Whatido;