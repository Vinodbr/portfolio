import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";
import Whatido from "@Home/whatido/whatido";
import About from "@Home/about/About";
import Accomplishments from "@Home/accomplishments/accomplishments";
import Skills from "@Home/skills/skills";
import Experience from "@Home/experience/experience";
import Link from '@material-ui/core/Link';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    "& > *": {
      margin: theme.spacing(0),
    },
    display: "flex",
    flexWrap: "wrap",
  padding: theme.spacing(1),
  },

  withBg:{
    backgroundColor:'black',
    marginLeft:theme.spacing(1)
  },

  [theme.breakpoints.down("lg")]: {
    withBg:{
      backgroundColor:'black',
      marginLeft:theme.spacing(1)
    },
  },

  photo:{
    width:'100%',
    height:'300px'
  },
  

  
  photoText:{
    //marginLeft:theme.spacing(6),
    color:'white',
    fontStyle:'italic',
    textAlign:"center"
  },

  [theme.breakpoints.down("lg")]: {
    photoText:{
      ///marginLeft:theme.spacing(0),
      color:'white',
      fontStyle:'italic'
    }
  },

  college:{
    display:'inline-block'
  },

  collegeIcon:{
    maxWidth:"125px",
    height:"125px",
  },

  [theme.breakpoints.only("sm")]: {
    collegeIcon:{
      maxWidth:theme.spacing(10),
      height:theme.spacing(10),
    },
  },

  
    [theme.breakpoints.down("xs")]: {
      collegeIcon:{
        maxWidth:theme.spacing(10),
        height:theme.spacing(10),
      },
    },
  

    [theme.breakpoints.down("sm")]: {
      photo:{
        width:"90%",
        height:"280px",
        marginLeft:theme.spacing(0)
      }
    },
  


}));

const Home = () => {
  const [spacing, setSpacing] = React.useState(0);
  const classes = useStyles();

  const handleChange = (event) => {
    setSpacing(Number(event.target.value));
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={2} alignItems='center' alignContent="center">
        <Grid item xs={12} sm={4} lg={3}>
          <Paper elevation={5} className={classes.withBg}>
            
            <img src="images/vinod1.jpg" alt="myphoto" className={classes.photo}/>
            <Typography variant="subtitle1" className={classes.photoText}>
              Engineering Manager
          </Typography>           
          </Paper>
        </Grid>        
        <Grid item xs={12} sm={8} lg={9} >          
          <About classes={classes}></About>           
        </Grid>
      </Grid>
      
      <Grid
        container
        spacing={1}
      >
        <Grid item xs={12} sm={6}>
          <Whatido classes={classes}></Whatido>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Accomplishments classes={classes}></Accomplishments>
        </Grid>
      </Grid>
      <Skills classes={classes}></Skills>
      
      <Experience classes={classes}></Experience> 
      <Grid container spacing={1} alignItems='center' alignContent="center">
        <Grid item xs={4} sm={2} lg={2}>
        <Typography color="primary" variant="subtitle2" gutterBottom>College:</Typography>        
        <img src="icons/sjbit.png" alt="sjbit" className={classes.collegeIcon}></img>
        </Grid>
        <Grid item xs={9} sm={7} lg={7}>
        <Typography >
        <Link href="https://sjbit.edu.in/" target="_blank" variant="subtitle1" color="textPrimary" underline="always" style={{marginLeft:"-10px"}}>
          SJB institute of technology (VTU)
        </Link>
        </Typography>
        <Typography color="textSecondary" variant="subtitle1" style={{marginLeft:"-10px"}} >Bachelor of Engineering in information science</Typography>
        </Grid>
        </Grid>
      </div>
  );
};

export default Home;
