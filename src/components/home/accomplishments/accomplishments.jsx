import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";

const itemsdata = [
  {
    text: "Reduced costs for AWS by 70% with ECS and spot instances and built a strong resilient system.",
  },
  {
    text: "Developed a 12-month Complex enterprise application just in 7 months by smartly leading a 8-member team of leads and engineers.",
  },
  {
    text: "Supported the org’s rapid growth by hiring new engineers and managers, and leveling up members of my team.",
  },
];

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,       
      },
    
      list:{
          marginTop :"-20px",
          marginLeft: "-10px"
      },
      listItem:{
        display : "list-item",
        marginBottom: "-35px",
        listStyleType: "square",
        marginLeft: "25px"
      },
}));

const Accomplishments = (props) => {
  const classes = useStyles();
  const parentClasses = props.classes;

  return (
    <>
      
          <Paper className={parentClasses.withBg} elevation={5}>
            <Typography color="primary" variant="subtitle2" gutterBottom>
              Recent Accomplishments:
            </Typography>
            <List component="ul" className={classes.list} ria-label="what i do">
              {itemsdata.map((value,index) => {
                const text = value.text;
                return (
                  <ListItem key={index} className={classes.listItem}>
                    <ListItemText>
                      <Typography variant="body2" paragraph gutterBottom>
                        {text}
                      </Typography>
                    </ListItemText>
                  </ListItem>
                );
              })}
            </List>
          </Paper>
        
    </>
  );
};

export default Accomplishments;
