import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,    
  },
  card:{
    maxWidth: 280,
    marginLeft: theme.spacing(1),
    marginTop: theme.spacing(0),
    display: "inline-block",
    height: '350px'
  },
  [theme.breakpoints.down("sm")]: {
    card:{
      maxWidth: 340,
      marginLeft: theme.spacing(2),
      marginTop: theme.spacing(0),
      display: "inline-block",
    },
},
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(2)',
  },
  secondCardHeader:{
    marginTop: theme.spacing(-3),
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  avatar: {
    backgroundColor: 'white',
  },
}));

const itemsdata=[
  {
    icon:"icons/bi2i.jpg",
    company:"Bridgei2i Analytics solutions Pvt Ltd",
    duration:"Aug 2019 - Jun 2021",
    designation:"Engineering Manager",
    domain:"Domain: I/ML",
    projects:[
      {
      client : "Client : SAFEbuilt US",
      project: "AI Solution to automate Residential plan review with CV, NLP technologies on AWS."
      },
      {
        client : "Client : IDFC bank",
        project: "AI Solution to detect fraud , gesture and extract relevant frames from Video KYC using CV on AWS."
      }
    ]
  },
  {
    icon:"icons/oracle.jpg",
    company:"Oracle India Pvt Ltd",
    duration:"Oct 2017 - Aug 2019",
    designation:"Principal member of technical staff",
    domain:"Domain: OCI Cloud IaaS",
    projects:[
      {
      client : "",
      project: "LBaaS: Load balancer as a service in Oracle OCI cloud. The Service provides automated traffic distribution from one entry point to multiple servers reachable from your virtual cloud network (VCN). The service offers a load balancer with customer choice of a public or private IP address, and provisioned bandwidth."
      }
    ]
  },
  {
    icon:"icons/hp.png",
    company:"Hewlett Packard Enterprise",
    duration:"Apr 2010 - Oct 2017",
    designation:"Specialist , Hewlett Packard labs",
    domain:"Domain: Storage",
    projects:[
      {
      client : "",
      project: "Data migration tool by discovering servers , switches and storages from legacy storage to new storage servers."
      },
      {
        client : "",
        project: "Watercooler enterprise social networking and knowledge exchange to make people and expertise more findable."
      }
    ]
  },
  {
    icon:"icons/hcl.png",
    company:"HCL Technologies",
    duration:"Jul 2006 - Mar 2010",
    designation:"Software Engineer",
    domain:"Domain: Banking and Financial services",
     projects:[ {
      client : "Client : SIDBI bank",
      project: "Portlet which integrates all the B-Spoke applications into single screen for the intranet site."
      },
      {
        client : "Client : SIDBI bank",
        project: "Automating the process of creating, managing and auditing various financial instruments."
      }
    ]
  }

]


export default function Experience() {
  const classes = useStyles();
  const bullet = <span className={classes.bullet}>•</span>;
  return (
    <>
     <div className={classes.root}>
     <Typography color="primary" variant="subtitle2" gutterBottom>Experience:</Typography>
      {itemsdata.map((item,index)=>{
        return(
        <Card className={classes.card} key={index}>
        <CardHeader
          avatar={
            <Avatar aria-label="company" className={classes.avatar} src={item.icon}/>
          }
          title={item.company}
          subheader={item.duration}
        ></CardHeader>
        <CardHeader
          className={classes.secondCardHeader}
          avatar={
              <>            
            <span style={{ marginLeft: '2.6rem',marginTop:'0.2rem' }}> </span>
            </>
          }
          title={item.designation}
          subheader={item.domain}
        ></CardHeader>
        <Divider  orientation="horizontal" />
        
          {item.projects.map((project,subIndex)=>{

            return(
              <CardContent key={subIndex}>
                <Typography variant="body2" color="textPrimary" component="p" >
                {project.client}
              </Typography>          
              <Typography variant="body2" color="textSecondary" component="p">
                {bullet}{" "}{project.project}
              </Typography>              
              </CardContent>
            );
          })}       
        
      </Card>);
      })}
      </div>
      
    </>
  );
}
