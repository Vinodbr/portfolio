import React from "react";
import { Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    "& > *": {
      margin: 0,
    },
  },
  image: {
    width: "80%",
    height: "300px",
  },
  fun:{
    width:"30px",
    height:"30px",
    marginTop:theme.spacing(1),
    marginLeft:theme.spacing(2),
  },
  carrom:{
    width: "80%",
    height: "300px",
  }
}));
const Fun = (props) => {
  const classes = useStyles();
  return (
    <>
      <Grid container spacing={3} alignItems="center">
        <Grid item xs={12} sm={3}>
          <img
            src="images/federer.jpg"
            alt="message1"
            className={classes.image}
          />
        </Grid>
        <Grid item xs={12} sm={3}>
          <Typography color="textPrimary" variant="h6">
            Big fan of Roger Federer!!
          </Typography>
        </Grid>
        <Grid item xs={12} sm={3}>
          <img
            src="images/gadgets.jpg"
            alt="message1"
            className={classes.image}
          />
        </Grid>
        <Grid item xs={12} sm={3}>
          <Typography color="textPrimary" variant="h6">
            Loves gadgets!!
          </Typography>
        </Grid>
      </Grid>
      <Grid container spacing={3} alignItems="center">
        <Grid item xs={12} sm={4}>
          <Paper>
            <div style={{display:"flex"}}>
            <img src="icons/spotify.png" alt="spotify" className={classes.fun}/> 
            <Typography color="textPrimary" variant="subtitle1" style={{marginTop:"6px",marginLeft:"8px"}}>
            Listen to music and podcasts on Spotify. 
            </Typography>
            </div>
            <div style={{display:"flex"}}>
            <img src="icons/netflix.jpg" alt="spotify" className={classes.fun}/>
            <Typography color="textPrimary" variant="subtitle1" style={{marginTop:"6px",marginLeft:"8px"}}>
             Watch webseries on Netflix. 
            </Typography>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={3}>
            <img src="images/carrom.jpg" alt="carrom" className={classes.carrom}/> 
        </Grid>
        <Grid item xs={12} sm={4}>          
            <Typography color="textPrimary" variant="h6">
             Play carrom with my kids!! 
            </Typography> 
        </Grid>
      </Grid>
    </>
  );
};

export default Fun;
