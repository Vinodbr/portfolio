import React from "react";
import { Typography } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import PhoneAndroidIcon from "@material-ui/icons/PhoneAndroid";
import EmailIcon from "@material-ui/icons/Email";
import CodeIcon from "@material-ui/icons/Code";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import NotesIcon from "@material-ui/icons/Notes";
import Link from "@material-ui/core/Link";
import InstagramIcon from "@material-ui/icons/Instagram";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  gridButton: {
    marginLeft: theme.spacing(1),
    marginTop: theme.spacing(0),
  },
}));
const Connect = (props) => {
  const classes = useStyles();
  const parentClasses = props.classes;
  return (
    <Grid
      container
      spacing={3}
      alignItems="center"
      alignContent="center"
      className={classes.gridButton}
    >
      <Grid item xs={10} sm={7}>
      <Typography
          component="div"
          className={classes.div}
          color="primary"
          variant="subtitle2"
          gutterBottom
        >
          Reach:
        </Typography>
        <Paper elevation={5}>
          <Button
            fullWidth
            variant="outlined"
            size="large"
            className={classes.button}
            startIcon={<PhoneAndroidIcon />}
          >
            <Link
              href="tel:+91-9886256832"
              target="_blank"
              variant="subtitle1"
              color="textPrimary"
              underline="always"
            >
              +91-9886256832
            </Link>
          </Button>
        </Paper>
      </Grid>
      <Grid item xs={10} sm={7}>
        <Paper elevation={5}>
          <Button
            fullWidth
            variant="outlined"
            style={{ textTransform: "lowercase" }}
            className={classes.button}
            startIcon={<EmailIcon />}
          >
            <Link
              href="mailto:br.vinu@gmail.com"
              target="_blank"
              variant="subtitle1"
              color="textPrimary"
              underline="always"
            >
              br.vinu@gmail.com
            </Link>
          </Button>
        </Paper>
      </Grid>
      
      <Grid item xs={10} sm={7}>
      <Typography
          component="div"
          className={classes.div}
          color="primary"
          variant="subtitle2"
          gutterBottom
        >
        This site code:
        </Typography>
        <Paper elevation={5}>
          <Button
            fullWidth
            variant="outlined"
            style={{ textTransform: "lowercase" }}
            className={classes.button}
            startIcon={<CodeIcon />}
          >
            <Link
              href="https://bitbucket.org/Vinodbr/portfolio"
              target="_blank"
              variant="subtitle1"
              color="textPrimary"
              underline="always"
            >
              https://bitbucket.org/Vinodbr/portfolio
            </Link>
          </Button>
        </Paper>
      </Grid>
      <Grid item xs={10} sm={4}>
      <Typography
          component="div"
          className={classes.div}
          color="textSecondary"
          variant="caption"
          gutterBottom
        >
        *Powered by ReactJS & Material UI
        </Typography>
      </Grid>
      <Grid item xs={10} sm={7}>
      <Typography
          component="div"
          className={classes.div}
          color="primary"
          variant="subtitle2"
        >
        Profile:
        </Typography>
        <Paper elevation={5}>
          <Button
            fullWidth
            variant="outlined"
            style={{ textTransform: "lowercase" }}
            className={classes.button}
            startIcon={<LinkedInIcon />}
          >
            <Link
              href="https://linkedin.com/in/vinodbr"
              target="_blank"
              variant="subtitle1"
              color="textPrimary"
              underline="always"
            >
              https://linkedin.com/in/vinodbr
            </Link>
          </Button>
        </Paper>
      </Grid>
      {/*<Grid item xs={10} sm={7}>
        <Paper elevation={5}>
          <Button
            fullWidth
            variant="outlined"
            style={{ textTransform: "lowercase" }}
            className={classes.button}
            startIcon={<NotesIcon />}
          >
            <Link
              href="https://medium.com/@br.vinu"
              target="_blank"
              variant="subtitle1"
              color="textPrimary"
              underline="always"
            >
              https://medium.com/@br.vinu
            </Link>
          </Button>
        </Paper>
  </Grid>*/}
      <Grid item xs={10} sm={7}>
        <Typography
          component="div"
          className={classes.div}
          color="primary"
          variant="subtitle2"
          gutterBottom
        >
          Timewall:
        </Typography>
        <Paper elevation={5}>
          <Button
            fullWidth
            variant="outlined"
            style={{ textTransform: "lowercase" }}
            className={classes.button}
            startIcon={<InstagramIcon />}
          >
            <Link
              href="https://www.instagram.com/brvinod/"
              target="_blank"
              variant="subtitle1"
              color="textPrimary"
              underline="always"
            >
              https://www.instagram.com/brvinod/
            </Link>
          </Button>
        </Paper>
      </Grid>
    </Grid>
  );
};
export default Connect;
