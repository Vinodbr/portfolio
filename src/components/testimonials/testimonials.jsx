import React from "react";
import { Typography } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      "& > *": {
        margin: 0,
      }
    },
    image:{
        width:"80%",
        height:"400px",
    }
  }));
const Testimonials = (props) => {
    const classes=useStyles();
  return (
    <>
      <Grid container spacing={3} alignItems="center">
        <Grid item xs={12} sm={12}>
        <Typography component="div" style={{ color: "white" }}>
        {" "}
        The following are some of the messages that I have received from people I worked with{" "}
      </Typography>
        </Grid>
        <Grid item xs={12} sm={12}>
        <img src="images/messages1.jpg" alt="message1" className={classes.image}/>  
        </Grid>
        <Grid item xs={12} sm={12}>
        <img src="images/messages2.jpg" alt="message2" className={classes.image}/>  
        </Grid>
      </Grid>
    </>
  );
};

export default Testimonials;
