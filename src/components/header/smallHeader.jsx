import React from "react";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import { withRouter } from "react-router-dom";
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import LabelIcon from '@material-ui/icons/Label';
import Avatar from '@material-ui/core/Avatar';
import { Typography } from "@material-ui/core";
import EmailIcon from '@material-ui/icons/Email';
import HomeIcon from '@material-ui/icons/Home';
import SportsTennisIcon from '@material-ui/icons/SportsTennis';
import PersonIcon from '@material-ui/icons/Person';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display:'inline-block'
  },
  typography:{
    marginLeft:theme.spacing(4)
  },
  
  list: {
    width: 220,
  },
  fullList: {
    width: 'auto',
  },
  avatar:{
    marginLeft: theme.spacing(10),
    color: theme.palette.primary.contrastText,
    backgroundColor: theme.palette.primary.main,
  },
  listItemText:{
    marginLeft: theme.spacing(6)
  }
}));

const SmallHeader = (props) => {
  const classes = useStyles();
  const parentClasses = props.classes;
  const open = props.open;
  const allMenus = props.allMenus;
  const setAllMenus = props.setAllMenus;
  const menuItems = props.menuItems;
  const handleButtonClick = props.handleButtonClick;
  const handleMenu = props.handleMenu;
  const handleMenuClick = props.handleMenuClick;
  const anchorEl = props.anchorEl;
  const setAnchorEl = props.setAnchorEl;
  const currentMenu = props.currentMenu
  

  const [state, setState] = React.useState({
    left: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };
  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === 'top' || anchor === 'bottom',
      })}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        <Avatar alt="Remy Sharp" src="/broken-image.jpg" className={classes.avatar}>
          VBR
        </Avatar>
          <ListItem>
            <ListItemText primary="Vinod B R" className={classes.listItemText} />
          </ListItem>
      </List>
      <Divider />

      <List>
        {menuItems.map((text, index) => (
          <ListItem button key={text} onClick={() => handleMenuClick(text.menuTitle,text.pageURL)}>
            <ListItemIcon>{<LabelIcon/>}</ListItemIcon>
            <ListItemText primary={text.menuTitle} />
          </ListItem>
        ))}
      </List>      
    </div>
  );
  return (
    <div className={classes.root}>
      {['left'].map((anchor) => (
        <React.Fragment key={anchor} >
          {/*<Button onClick={toggleDrawer(anchor, true)}>{anchor}</Button>*/}
          <IconButton
        edge="start"
        className={classes.menuButton}
        color="inherit"
        aria-label="menu"
        onClick={toggleDrawer(anchor, true)}
      >
        <MenuIcon />
        <Typography variant="subtitle1" className={classes.typography}>{currentMenu}</Typography>
      </IconButton>
          <SwipeableDrawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
            onOpen={toggleDrawer(anchor, true)}
          >
            {list(anchor)}
          </SwipeableDrawer>
          
        </React.Fragment>
      ))}
      

    </div>
  );
};

export default withRouter(SmallHeader);
