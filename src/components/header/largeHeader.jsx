import React from "react";
import { withRouter } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import { Typography } from "@material-ui/core";

const LargeHeader = (props) => {
  const classes = props.classes;
  const allMenus = props.allMenus;
  const handleButtonClick = props.handleButtonClick;

  return (
    <div className={classes.root}>
      <Grid
        container
        spacing={1}
        direction="row"
        justifyContent="flex-end"
        alignItems="center"
      >
        <Grid item xs={12} sm={2}>
          <Link
            component="button"
            underline={allMenus.home ?"always":"none"}
            className={
              allMenus.home ? classes.menuSelected : classes.menuInactive
            }
            onClick={(e) => handleButtonClick(e, "Home" ,"/", "home", true)}
          >
          <Typography variant="subtitle1" gutterBottom>{/*<HomeIcon style={{verticalAlign:'top'}}/> you will have to remove gutter bottom and try*/}Home</Typography>
          </Link>
        </Grid>

        <Grid item xs={1} sm={2}>
          <Link
            component="button"
            underline={allMenus.connect ?"always":"none"}
            className={
              allMenus.connect ? classes.menuSelected : classes.menuInactive
            }
            onClick={(e) => handleButtonClick(e,"Connect","/connect", "connect", true)}
          >
            <Typography variant="subtitle1" gutterBottom>Connect</Typography>
          </Link>
        </Grid>

        <Grid item xs={1} sm={2}>
          <Link
            component="button"
            underline={allMenus.testimonials ?"always":"none"}
            className={
              allMenus.testimonials ? classes.menuSelected : classes.menuInactive
            }
            onClick={(e) => handleButtonClick(e,"Testimonials", "/testimonials", "testimonials", true)}
          >
            <Typography variant="subtitle1" gutterBottom>Testimonials</Typography>
          </Link>
        </Grid>
        <Grid item xs={1} sm={2}>
          <Link
            component="button"
            underline={allMenus.fun ?"always":"none"}
            className={
              allMenus.fun ? classes.menuSelected : classes.menuInactive
            }
            onClick={(e) => handleButtonClick(e,"Fun", "/fun", "fun", true)}
          >
            <Typography variant="subtitle1" gutterBottom>Fun</Typography>
          </Link>
        </Grid>
      </Grid>
    </div>
  );
};
export default withRouter(LargeHeader);
