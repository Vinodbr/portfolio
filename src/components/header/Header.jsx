import React, { useState, useEffect,useRef } from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import { withRouter } from "react-router-dom";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import LargeHeader from "@Header/largeHeader";
import SmallHeader from "@Header/smallHeader";
import Banner from "@Banner/banner";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    "& > *": {
      margin: 0,
    }
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    [theme.breakpoints.down("xs")]: {
      flexGrow: 1,
    },
  },
  menuSelected: {
    color: "white",
  },
  menuInactive: {
    color: "black",
  },
  customizeToolbar: {
    minHeight: 36,
  }
}));

function usePrevious(value,history,path) {
  // The ref object is a generic container whose current property is mutable ...
  // ... and can hold any value, similar to an instance property on a class
  const ref = useRef();
  // Store current value in ref
  //console.log("onclick of back");
  //console.log(ref.current)
  //console.log("value")
  //console.log(value);
  
  const action = history.action;
     if (action === "POP") {
        const identifiers = Object.keys(value)
        const active = identifiers.filter(function(id) {
          return value[id]
        })
        active.map((item)=>{
          if(item===path)
          {
            console.log('dont do anything');
          }
          else
          {
            value[item]=false;
          }
        })
     }
  ref.current=value;
  //will be called when there is a state change only and will be called first before useeffect after mount internally.
  useEffect(() => {
    ref.current = value;
  }, [value]); // Only re-run if value changes
  // Return previous value (happens before update in useEffect above)
  return ref.current;
}

function usePreviousMenu(value,history,path) {
  // The ref object is a generic container whose current property is mutable ...
  // ... and can hold any value, similar to an instance property on a class
  const ref = useRef();
   
  const action = history.action;
     if (action === "POP") {
        value="Home";
        if(path === "connect"){
          value="Connect";
        }
        else if(path==="testimonials"){
          value="Testimonials";
        }
        else if(path==="fun"){
          value="Fun";
        }
     }
  ref.current=value;
  //will be called when there is a state change only and will be called first before useeffect after mount internally.
  useEffect(() => {    
    ref.current = value;
  }, [value]); // Only re-run if value changes
  // Return previous value (happens before update in useEffect above)
  return ref.current;
}

const menuItems = [
  {
    menuTitle: "Home",
    pageURL: "/",
  },
  {
    menuTitle: "Connect",
    pageURL: "/connect",
  },    
  {
    menuTitle: "Testimonials",
    pageURL: "/testimonials",
  },
  {
    menuTitle: "Fun",
    pageURL: "/fun",
  },
];


const Header = (props) => {
  const { history } = props;
  const initialState = {
    home: false,    
    connect: false,
    testimonials: false,
    fun:false,
  };
  let [allMenus, setAllMenus] = useState(initialState);
  let [currentMenu,setCurrentMenu]=useState("Home");
  const str = history.location.pathname;
    let path = "home";
      if (str === "/") {
        path = "home";
      } else {
        path = str.substring(1);
      }
  allMenus[path]=true;
  //console.log(allMenus);
  const prevallMenus= usePrevious(allMenus,history,path);
  allMenus=prevallMenus;

  const prevMenu=usePreviousMenu(currentMenu,history,path)
  currentMenu=prevMenu;
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("xs"));

  /* called after mounting, this happens with this header component and happens only when user try typing url in the browser*/
  useEffect(() => {
   // alert('called after mount');
    const history = props.history;
    const str = history.location.pathname;
    let path = "home";
      if (str === "/") {
        path = "home";
      } else {
        path = str.substring(1);
      }
     
      const initialState = {
        home: false,    
        connect: false,
        testimonials: false,
        fun:false,
      };
     setAllMenus({ ...initialState, [path]: true });
  },[]);

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClick = (menuTitle, pageURL) => {
    history.push(pageURL);
    setCurrentMenu(menuTitle);
    setAnchorEl(null);
  };

  const handleButtonClick = (e, menuTitle,pageURL, name, selected) => {
    history.push(pageURL);
    //setAllMenus(() => ({...initialState, [name]: selected}));
    const initialState = {
      home: false,    
      connect: false,
      testimonials: false,
      fun:false,
      
    };
    setAllMenus({ ...initialState, [name]: selected });
  };

  

  return (
    <div className={classes.root}>
      {isMobile ? (<></>):(<Banner></Banner>)}
      <AppBar position="static">
        <Toolbar className={classes.customizeToolbar} disableGutters>
          {true && (
            <>
              {isMobile ? (
                <>
                  <SmallHeader classes={classes} open={open} currentMenu={currentMenu} allMenus={allMenus} setAllMenus={setAllMenus} menuItems={menuItems} handleButtonClick={handleButtonClick} handleMenu={handleMenu} handleMenuClick={handleMenuClick} anchorEl={anchorEl} setAnchorEl={setAnchorEl} />
                </>
              ) : (
                <>
                  <LargeHeader classes={classes} history={history} currentMenu={currentMenu} allMenus={allMenus} setAllMenus={setAllMenus} handleButtonClick={handleButtonClick} />
                </>
              )}
            </>
          )}
        </Toolbar>
      </AppBar>
    </div>
  );
};
export default withRouter(Header);
