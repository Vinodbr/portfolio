import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",    
    overflow: "hidden",
    height: "60px",
    marginLeft: theme.spacing(1),
    background: 'linear-gradient(170deg, #000  30%, #fbce07 90%)',
    verticalAlign:'bottom'
  },
  imageList: {
    flexWrap: "nowrap",
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: "translateZ(0)",
  },
  title: {
    color: theme.palette.primary.light,
  },
  titleBar: {
    background:
      "linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)",
  },
  text:{
    marginTop:theme.spacing(2),
  }
}));

const Banner = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
    <Typography
      variant="h6"
      component="div"      
      color="textPrimary"
      align="center"
      className={classes.text}
    >
      Vinod B R
    </Typography>
    </div>
  );
};
export default Banner;
