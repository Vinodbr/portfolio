# My Portfolio React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Deployment
- This project is deployed on AWS cloud(free tier).
- S3 bucket for static files
- Cloudfront distribution fronting S3.
- AWS codepipeline setup for CI/CD. check buildspec.frontend.yml
